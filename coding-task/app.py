import json
import logging
import threading
from flask import Flask, render_template, make_response
from monitor import monitor_periodically


app = Flask(__name__)
app.config['SECRET_KEY'] = 'api-secret-key-xsdf-dsfa-hgef'
app.config['REPORT_FILE'] = 'report.json'
logger = logging.getLogger()
logger.setLevel(logging.INFO)


@app.route('/', methods=['GET'])
def index():
    report = {}
    try:
        with open(app.config.get('REPORT_FILE', 'report.json')) as fp:
            report = json.load(fp)
    except (OSError, IOError) as ex:
        logger.critical("Unable to open the monitoring report.")
        logger.info("Encountered exception: {}".format(ex))
        return make_response(500, "Encountered error generating the report.")
    return render_template('monitoring-status.html', report=report)


@app.route('/demo1', methods=['GET'])
def demo1():
    """
    This endpoint is only meant for local testing.
    Will ideally not be present in actual production tool.
    :return:
    """
    return make_response("This is demo1 webpage.", 200)


@app.route('/demo2', methods=['GET'])
def demo2():
    """
    This endpoint is only meant for local testing.
    Will ideally not be present in actual production tool.
    :return:
    """
    return make_response("This is demo2 webpage.", 200)


if __name__ == '__main__':
    with open("config.json") as cfg:
        config = json.load(cfg)
    thread = threading.Thread(target=monitor_periodically, args=(config, ))
    thread.setDaemon(True)
    thread.start()
    app.run(host=config["server"]["bind"],
            port=int(config["server"]["port"]),
            debug=True)
