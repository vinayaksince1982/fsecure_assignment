# Tool to monitor a set of websites.(Coding task)

* Used flask to provide a endpoint which renders the html webpage.
* List of http urls to be monitored can be passed into a file called config.json.
* It also accepts a regex pattern that can be used to match text on the webpage retured by querying the http endpoint from list above.
* This can use celery for the background task. But used a thread to keep things simple and dependencies low in the assignment.


## Setup instructions:
1. Needs python3.
2. Assume pip is installed.
3. Create and activate a virtualenv
4. pip install -r requirements.txt
5. Run the flask application server: python app.py
6. Open http://localhost:8080 to observe the status of websites being monitored.
7. Also checked-in a sample report.json, which is generated after each query interval specified in the config.

# Design Task:

Extend it to collect data from geographically distributed locations and collate the data.
***
1. We will need an additional field called "location" which reflects source of data.
2. I am running the data collector as thread as a part of this web application, the data collector will need to be separated from the rest api. 
3. An additional endpoint can be added which accepts data posted from other locations.
4. The server collates the data into a single report and is available as html.
5. The data may not be sycnhronized to the second, but can reflect the latest status from each location.
6. We will need to enable https on this server and atleast basic authentication so the rest endpoints are protected.
7. Additionally data can be persisted to a DB, to do uptime analysis over a period of time.
