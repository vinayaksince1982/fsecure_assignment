import json
import logging
import re
import requests
import time

logger = logging.getLogger()


def get_website_status(url):
    """
    Returns the status code, response time & contents of the webpage
    :param url: String containing url to hit.
    :return:
    """
    start = time.time()
    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as ex:
        logger.critical("Unable to fetch: {}".format(url))
        logger.info("Encountered exception: {}".format(ex))
        return 0, time.time() - start, ""
    return response.status_code, time.time() - start, response.text


def monitor_websites(urls, logfile):
    report = {}
    report["urls"] = []
    with open(logfile, "a") as fp:
        for d in urls:
            status_code, duration, text = get_website_status(d["url"])
            m = re.match(d["pattern"], text)
            status = "Down"
            if m:
                status = "Live"
            fp.write("url: {} load-time: {}, status: {}\n"
                     .format(d["url"], duration, status))
            curr_url_status = {}
            curr_url_status["url"] = d["url"]
            curr_url_status["status"] = status
            curr_url_status["load_time"] =str(duration)
            report["urls"].append(curr_url_status)
        report["last_updated"] = time.strftime("%m/%d/%Y, %H:%M:%S")
    return report


def monitor_periodically(config):
    while True:
        report = monitor_websites(config["monitored_endpoints"],
                                  config["log_file"])
        try:
            with open(config["report_file"], "w") as fp:
                json.dump(report, fp)
        except (OSError, IOError) as ex:
            logger.critical("Encountered error writing to file {}"
                            .format(config["report_file"]))
            logger.info("Encountered exception: {}".format(ex))

        time.sleep(int(config["monitoring_interval"]))